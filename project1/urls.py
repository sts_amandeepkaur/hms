"""project1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from app1 import views

from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.index,name='index'),
    path('about/',views.about,name='about'),
    path('register/',views.registeration,name='register'),
    path('contact/',views.contactus,name='pcontact'),
    path('signin/',views.signin,name='signin'),
    path('dashboard/',views.dashboard,name='dashboard'),
    path('uslogout/',views.uslogout,name='uslogout'),
    path('st_details/',views.st_details,name='st_details'),
    path('rooms/',views.rooms,name='rooms'),
    path('add_student/',views.add_student,name='add_student'),
    path('empty_room/',views.empty_room,name='empty_room'),
    path('delete_student/',views.delete_student,name='delete_student'),
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
