from django.db import models

class register(models.Model):

    subtitle = models.CharField(max_length=250,null=True)
    name = models.CharField(max_length=250)
    fathername = models.CharField(max_length=250)
    mothername = models.CharField(max_length=250)
    email = models.EmailField()
    mobileno = models.IntegerField()
    branch = models.CharField(max_length=250)
    semester = models.CharField(max_length=250)
    rollno = models.IntegerField(null=True)
    permanentaddress = models.CharField(max_length=250)
    resultproof = models.FileField(upload_to="results/%Y/%m/%d")
    photo = models.FileField(upload_to="photo/%Y/%m/%d")
    hostel_fee = models.FileField(upload_to="hostelfee/%Y/%m/%d",null=True)
    electric_fee = models.FileField(upload_to="electric/%Y/%m/%d",null=True)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    is_approved= models.BooleanField(default=False,null=True)
    is_allocated= models.BooleanField(default=False,null=True)


    def __str__(self):
        return self.name

class room(models.Model):
    C=(
        ('SBH','Senior Boys Hostel'),('JBH','Junior Boys Hostel'),('GH','Girls Hostel')
    )
    hostel_type = models.CharField(max_length=300,choices=C)
    room_no = models.CharField(max_length=250)
    student1 = models.ForeignKey(register,on_delete=models.CASCADE,related_name="student1",blank=True,null=True)
    student2 = models.ForeignKey(register,on_delete=models.CASCADE,related_name="student2",blank=True,null=True)
    student3 = models.ForeignKey(register,on_delete=models.CASCADE,related_name="student3",blank=True,null=True)
    student4 = models.ForeignKey(register,on_delete=models.CASCADE,related_name="student4",blank=True,null=True)
    student5 = models.ForeignKey(register,on_delete=models.CASCADE,related_name="student5",blank=True,null=True)
    added_on = models.DateTimeField(auto_now_add=True)
    is_available = models.BooleanField(default=True)
    total = models.IntegerField(null=True,default=0)
    def __str__(self):
        return self.room_no

class contact(models.Model):
    sub = models.CharField(max_length=250,blank=True)
    first_name = models.CharField(max_length=250,blank=True)
    last_name = models.CharField(max_length=250,blank=True)
    message = models.TextField(blank=True)
    added_on = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.first_name