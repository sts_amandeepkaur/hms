from django.shortcuts import render
from app1.models import register,room, contact
from django.contrib.auth import login,logout,authenticate
from django.http import HttpResponseRedirect, HttpResponse

from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMessage
# Create your views here.
def index(r):
    return render(r,'page1.html')

def about(r):
    return render(r,'about.html')

def rooms(r):
    if 'type' in r.GET:
        tp = r.GET['type']
        data = room.objects.filter(hostel_type=tp)
        return render(r,'rooms.html',{'rooms':data})
    return render(r,'rooms.html')

def registeration(r):
    if r.method=="POST":
        nm = r.POST['name']
        sb = r.POST['Subtitle']
        fn = r.POST['fathername']
        mn = r.POST['mothername']
        em = r.POST['email']
        mb = r.POST['mobileno']
        br = r.POST['branch']
        sm = r.POST['semester']
        ad = r.POST['permanentaddress']
        rl = r.POST['rollno']
        data = register(name=nm,fathername=fn,subtitle=sb,mothername=mn,email=em,mobileno=mb,branch=br,semester=sm,permanentaddress=ad,rollno=rl)
        data.save()
        if 'hostelfee' in r.FILES:
            hf = r.FILES['hostelfee']
            rp = r.FILES['resultproof']
            ef = r.FILES['electricfee']
            p = r.FILES['photo']

            data.hostel_fee=hf
            data.resultproof=rp
            data.electric_fee=ef
            data.photo=p
            data.save()
        return render(r,'register.html',{'status':'Dear {}, thanks for registration, we will contact you soon'.format(nm)})

    return render(r,'register.html')

def contactus(r):
    if r.method=="POST":
        sb = r.POST['sub']
        fn = r.POST['fn']
        ln = r.POST['ln']
        ms = r.POST['msz']
        data = contact(sub=sb,first_name=fn,last_name=ln,message=ms)
        data.save()
        st = "Dear {} {} {} thanks for your feedback!!!".format(sb,fn,ln)
        return render(r,'pcontact.html',{'status':st})  
    return render(r,'pcontact.html')  

def signin(r):
    if r.method=="POST":
        un = r.POST['un']
        pwd = r.POST['pas']
        check = authenticate(username=un,password=pwd)
        if check:
            if check.is_active:
                login(r,check)
                return HttpResponseRedirect('/dashboard')
            else:
                return render(r,'login.html',{'status':'Sorry You are not allowed!!'})
        else:
            return render(r,'login.html',{'status':'Invalid Username or Password!!'})
    return render(r,'login.html')

@login_required  
def dashboard(request):
    rg = register.objects.filter(is_approved=False).order_by('-id')
    if 'status' in request.GET:
        sts = request.GET['status']
        return render(request,'dashboard.html',{'all':rg,'total':len(rg),'status':sts})
    if 'rid' in request.GET:
        rd=[]
        r_id = request.GET['rid']
        rm = room.objects.get(id=r_id)
        if rm.student1 is not None:
            rd.append(rm.student1)
        if rm.student2 is not None:
            rd.append(rm.student2)
        if rm.student3 is not None:
            rd.append(rm.student3)
        if rm.student4 is not None:
            rd.append(rm.student4)
        if rm.student5 is not None:
            rd.append(rm.student5)
        return render(request,'dashboard.html',{'all':rd,'total':len(rd),'room':rm})
    
    return render(request,'dashboard.html',{'all':rg,'total':len(rg)})

def delete_student(request):
    if 'rid' in request.GET:
        rs=''
        sid = request.GET['sid']
        rid = request.GET['rid']
        rm = room.objects.get(id=rid)
        st = register.objects.get(id=sid)
        if rm.student1 is not None:
            if rm.student1.id==st.id:
                rm.student1 = None
                rm.total-=1
                rm.save()
                st.delete()
                rs ='Deleted Successfully'
        if rm.student2 is not None:
            if rm.student2.id==st.id:
                rm.student2 = None
                rm.total-=1
                rm.save()
                st.delete()
                rs ='Deleted Successfully'
        if rm.student3 is not None:
            if rm.student3.id==st.id:
                rm.student3 = None
                rm.total-=1
                rm.save()
                st.delete()
                rs ='Deleted Successfully'
        if rm.student4 is not None:
            if rm.student4.id==st.id:
                rm.student4 = None
                rm.total-=1
                rm.save()
                st.delete()
                rs ='Deleted Successfully'
        if rm.student5 is not None:
            if rm.student5.id==st.id:
                rm.student5 = None
                rm.total-=1
                rm.save()
                st.delete()
                rs ='Deleted Successfully'
        return HttpResponseRedirect('/dashboard/?rid='+str(rm.id)+'&&status='+rs)

@login_required  
def uslogout(request):
    logout(request)
    return HttpResponseRedirect('/')
@login_required  
def st_details(request):
    if request.method=="POST":
        sid=request.POST['sid']
        st = register.objects.get(id=sid)
        if "approve" in request.POST:
            st.is_approved=True
            st.save()
            rs = "{} Approved Successfully".format(st.name)
            try:
                msz = "Dear {} your hostel application approved successfully!!! You will receive your room detail soon!!".format(st.name)
                data = EmailMessage("Hostel Request Action",msz,to=[st.email,])
                data.send()
            except:
                return HttpResponse("<h1>Incorrect Email ID or check your Internet Connection</h1>")
            return render(request,'details.html',{'status':rs})
        if "reject" in request.POST:
            st.is_approved=False
            st.save()
            rs = "{} Rejected Successfully".format(st.name)
            try:
                msz = "Dear {} your hostel application rejected!! You can contact to hostel faculty member for any query!!".format(st.name)
                data = EmailMessage("Hostel Request Action",msz,to=[st.email,])
                data.send()
                st.delete()
            except:
                return HttpResponse("<h1>Incorrect Email ID or check your Internet Connection</h1>")
            
            return render(request,'details.html',{'status':rs})
    
    if 'room' in request.GET:
        rm = room.objects.get(id=request.GET['room'])
        id = request.GET['id']
        data = register.objects.get(id=id)
        return render(request,'details.html',{'d':data,'room':rm})

    if 'id' in request.GET:
        id = request.GET['id']
        data = register.objects.get(id=id)
        return render(request,'details.html',{'d':data})
    return render(request,'details.html')
def add_student(request):
    id = request.GET['rid']
    rm = room.objects.get(id=id)
    data = register.objects.filter(is_approved=True,is_allocated=False)
    nospace=0
    if request.method=="POST":
        ls=request.POST.getlist('add[]')
        rid = request.POST['rid']
        room_obj =room.objects.get(id=rid)
        ## Remove Students 
        if room_obj.student1 is not None:
            if str(room_obj.student1.id)  not in ls:
                uid = room_obj.student1.id
                rg=register.objects.get(id=uid)
                room_obj.student1 = None
                room_obj.total-=1
                room_obj.save()
                rg.is_allocated=False
                rg.save()
        if room_obj.student2 is not None:
            if str(room_obj.student2.id)  not in ls:
                uid = room_obj.student2.id
                rg=register.objects.get(id=uid)
                room_obj.student2 = None
                room_obj.total-=1
                room_obj.save()
                rg.is_allocated=False
                rg.save()
        if room_obj.student3 is not None:
            if str(room_obj.student3.id)  not in ls:
                uid = room_obj.student3.id
                rg=register.objects.get(id=uid)
                room_obj.student3 = None
                room_obj.total-=1
                room_obj.save()
                rg.is_allocated=False
                rg.save()
        if room_obj.student4 is not None:
            if str(room_obj.student4.id)  not in ls:
                uid = room_obj.student4.id
                rg=register.objects.get(id=uid)
                room_obj.student4 = None
                room_obj.total-=1
                room_obj.save()
                rg.is_allocated=False
                rg.save()
        if room_obj.student5 is not None:
            if str(room_obj.student5.id)  not in ls:
                uid = room_obj.student5.id
                rg=register.objects.get(id=uid)
                room_obj.student5 = None
                room_obj.total-=1
                room_obj.save()
                rg.is_allocated=False
                rg.save()
        
        # ADD STUDENT IN ROOM
        nospace=0
        usrs=[]
        for i in ls:
            st = register.objects.get(id=i)
            if st.is_allocated==False:
                if room_obj.student1==None:
                    room_obj.student1=st
                    room_obj.total+=1
                    room_obj.save()
                    st.is_allocated=True
                    st.save()
                    usrs.append(st.email)
                elif room_obj.student2==None:
                    room_obj.student2=st
                    room_obj.total+=1
                    room_obj.save()
                    st.is_allocated=True
                    st.save()
                    usrs.append(st.email)
                elif room_obj.student3==None:
                    room_obj.student3=st
                    room_obj.total+=1
                    room_obj.save()
                    st.is_allocated=True
                    st.save()
                    usrs.append(st.email)
                elif room_obj.student4==None:
                    room_obj.student4=st
                    room_obj.total+=1
                    room_obj.save()
                    st.is_allocated=True
                    st.save()
                    usrs.append(st.email)
                elif room_obj.student5==None:
                    room_obj.student5=st
                    room_obj.total+=1
                    room_obj.save()
                    st.is_allocated=True
                    st.save()
                    usrs.append(st.email)
                else:
                    nospace=1
        
        if nospace==1:
            st = "There is no more space in this room"
        else:
            st = "Updated Successfully"
            if len(usrs)>0:
                try:
                    msz = "Romm allocation success!!!\n {} - {} is your room number. Contact staff member for more details".format(rm.room_no,rm.hostel_type)
                    ml = EmailMessage("Room Allocation",msz,to=usrs)
                    ml.send()
                except:
                    return HttpResponse("<h1>Incorrect Email ID or check your Internet Connection</h1>")
            return render(request,'addstudent.html',{'all':data,'room':rm,'status':st})
        return render(request,'addstudent.html',{'all':data,'room':rm,'status':st}) 
    return render(request,'addstudent.html',{'all':data,'room':rm})     

def empty_room(request):    
    id = request.GET['rid']
    rm = room.objects.get(id=id)
    if 'ac' in request.GET:     
        act = request.GET['ac']
        if act=="true":
            if rm.student1 is not None:
                rm.student1.is_allocated=False
                rm.student1.save()
                rm.student1=None
                rm.total-=1
                rm.save()
            if rm.student2 is not None:
                rm.student2.is_allocated=False
                rm.student2.save()
                rm.student2=None
                rm.total-=1
                rm.save()
            if rm.student3 is not None:
                rm.student3.is_allocated=False
                rm.student3.save()
                rm.student3=None
                rm.total-=1
                rm.save()
            if rm.student4 is not None:
                rm.student4.is_allocated=False
                rm.student4.save()
                rm.student4=None
                rm.total-=1
                rm.save()
            if rm.student5 is not None:
                rm.student5.is_allocated=False
                rm.student5.save()
                rm.student5=None
                rm.total-=1
                rm.save()
            return HttpResponseRedirect('/rooms/?type='+rm.hostel_type)
        return render(request,'empty.html',{'room':rm})
    return render(request,'empty.html',{'room':rm})