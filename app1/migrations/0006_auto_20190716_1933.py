# Generated by Django 2.2.1 on 2019-07-17 02:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0005_register_rollno'),
    ]

    operations = [
        migrations.AddField(
            model_name='register',
            name='is_allocated',
            field=models.BooleanField(default=False, null=True),
        ),
        migrations.AddField(
            model_name='register',
            name='is_approved',
            field=models.BooleanField(default=False, null=True),
        ),
        migrations.CreateModel(
            name='room',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hostel_type', models.CharField(choices=[('SBH', 'Senior Boys Hostel'), ('JBH', 'Junior Boys Hostel'), ('GH', 'Girls Hostel')], max_length=300)),
                ('room_no', models.CharField(max_length=250)),
                ('added_on', models.DateTimeField(auto_now_add=True)),
                ('is_available', models.BooleanField(default=True)),
                ('student1', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='student1', to='app1.register')),
                ('student2', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='student2', to='app1.register')),
                ('student3', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='student3', to='app1.register')),
                ('student4', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='student4', to='app1.register')),
                ('student5', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='student5', to='app1.register')),
            ],
        ),
    ]
