from django.contrib import admin
from app1.models import register,room
# Register your models here.

class roomAdmin(admin.ModelAdmin):
    list_display=['hostel_type','room_no']
    search_fields = ['room_no']
admin.site.register(register)
admin.site.register(room,roomAdmin)
